function fazerPergunta() {
    let random = Math.floor(Math.random() * 8) + 1;

    document.getElementById('imagem-resposta')
        .src = "images/resposta_".concat(random).concat('.png');
}

function reiniciar() {
    document.getElementById('imagem-resposta').src = "images/inicio.png";
    alert('Vamos recomeçar!\nFaça uma nova pergunta para a Magic 8 Ball!');
}